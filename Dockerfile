FROM openjdk:11z-alpine3.7
RUN apk --no-cache add curl
COPY build/libs/*-all.jar pader-streets.jar
CMD java ${JAVA_OPTS} -jar pader-streets.jar